<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
//     // return 'Hello laravel';
    

// });

// Authentication 
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/','PostController@index');
// Route::get('post/{id}','PostController@show');
Route::get('/upload','PostController@create');
Route::post('/upload','PostController@store');
Route::get('/post/{post}','PostController@show');
Route::get('/post/delete/{post}','PostController@destroy');
Route::get('/post/edit/{post}','PostController@edit');
Route::post('/post/update','PostController@update');



Route::get('/add_category','CategoryController@create');
Route::post('/addcategory','CategoryController@store');
Route::get('/categories','CategoryController@index');
Route::get('/category/delete/{category}','CategoryController@destroy');
Route::get('/category/edit/{category}','CategoryController@edit');
Route::post('/category/update','CategoryController@update');



 Route::post('/comment','CommentController@create');


 Route::get('/admin-login','Auth\AdminLoginController@showLoginFrom');
 Route::post('/admin-login','Auth\AdminLoginController@login');
 Route::get('/backend','AdminController@index');
