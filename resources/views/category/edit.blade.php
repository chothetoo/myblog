@extends('layouts.template')
@section('content')
<div class="col-md-8">
	<h2>Edit</h2>
	<form method="post" action="/category/update">
		@csrf
		<input type="hidden" name="cat_id" value="{{$category->id}}">
		<div class="form-group">
			<label>Category Name:</label>
			<input type="text" name="category" class="from-control" value="{{$category->category_name}}">
		</div>
		<div class="form-group">
			<input type="submit" name="btnsubmit" value="Update" class="btn btn-primary">
		</div>
	</form>
	
</div>
@endsection