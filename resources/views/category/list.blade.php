@extends('layouts.template')
@section('content')
<div class="col-md-8">
	<h2>Category List</h2>

	<!-- @if(isset($msg))
	<div class="alert alert-info">{{$msg}}</div>
	@endif -->

	<a href="add_category" class="btn btn-primary"> Add </a>
	<table class="table">
		<thead>
			<tr>
				<th>No.</th>
				<th>Name</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			@foreach($categories as $category)
			<tr>
				<td>{{$loop->iteration}}</td>
				<td>{{$category->category_name}}</td>
				<td><a href="/category/edit/{{$category->id}}" class="btn btn-warning">Edit</a></td>
				<td><a href="/category/delete/{{$category->id}}" class="btn btn-danger">Delete</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection