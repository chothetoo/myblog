@extends('layouts.template')

@section('content')
<div class="col-md-8">

	<table class="table mp-3">
		<thead>
			<tr>
				<th>No.</th>
				<th>Name</th>
			</tr>
		</thead>
		<tbody>
			 @foreach($categories as $category)
			 <tr>
			 <td>{{$category->id }}</td>
			 <td>{{$category->category_name }}</td>
			 <tr>
			 @endforeach
			
		</tbody>
		
	</table>
	<a href="/add" class="btn btn-primary">Add new Categories</a>
</div>




@endsection