@extends('layouts.template')

@section('content')
<div class="col-md-8">
	@if(count($errors))
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{ $error}}</li>
			@endforeach
		</ul>
	</div>
		@endif
		<h2>Add new category</h2>

		<form action="/addcategory" method="post">
			@csrf
			<div class="form-group my-5">
				<label>Category Name:</label>
				<input type="text" name="category" class="form-control">		
			</div>
			<div class="form-group">
				<input type="submit" name="btnsubmit" value="Add" class="btn-outline-primary">
			</div>
				</form>
			</div>
		

@endsection
