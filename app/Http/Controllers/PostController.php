<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\User;

class PostController extends Controller
{
    public function __construct(){
        $this->middleware('auth',['except'=>['index','show']]);
    }




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts=Post::all();
        return view('blog',compact('posts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('posts.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'title'=>'required|min:5',
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'body'=>'required|min:10'
        ]);

        $imageName=time().'.'.request()->photo->getClientOriginalExtension();
        request()->photo->move(public_path('images'),$imageName);
        $fullImg = '/images/'.$imageName;


        Post::create([
            'title'=>request('title'),
            'category_id'=>request('category'),
            'photo'=>$fullImg,
            'body'=>request('body'),
            'user_id'=>auth()->id()
        ]);

        // dd($request);
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)

    {
        //dd($post);
        return view('blogpost',compact('post'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories=Category::all();

        return view('posts.edit',compact('post','categories'));

        

              
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=request('post_id');
        $post=Post::find($id);
        $post->title=request('title');
        $post->category_id=request('category');

        if(request('photo')):
            $imgName=time().'.'.request()->photo->getClientOriginalExtension();
            request()->photo->move(public_path('images'),$imgName);
            $fullImg="/images/".$imgName;
            $post->photo=$fullImg;

        endif;

        $post->body=request('body'); 
         
        $post->save();
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        // foreach ($post->comments as $comment): 
        //     $comment->delete();
        // endforeach;
        $post->delete();


        // Post::delete($post->id);
       return redirect('/');
    }
}
